package sample;

import javafx.beans.property.SimpleStringProperty;

public class PlayList {
    private SimpleStringProperty playListName;
    private int ownerID;
    private int playlistID;
    public PlayList(String playListName, int ownerID, int playlistID){
        this.playListName = new SimpleStringProperty(playListName);
        this.ownerID = ownerID;
        this.playlistID = playlistID;
    }

    public String getPlayListName() {
        return playListName.get();
    }

    public SimpleStringProperty playListNameProperty() {
        return playListName;
    }

    public void setPlayListName(String playListName) {
        this.playListName.set(playListName);
    }

    public int getPlaylistID() {
        return playlistID;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }
}
