package sample;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Song {
    private SimpleStringProperty name;
    private SimpleStringProperty albumName;
    private SimpleStringProperty seconds;
    private String filePath;
    private int songID;

    public Song(String name, String albumName, String seconds, String filePath, int songID){
        this.name = new SimpleStringProperty(name);
        this.albumName = new SimpleStringProperty(albumName);
        this.seconds = new SimpleStringProperty(seconds);
        this.filePath = filePath;
        this.songID = songID;
    }


    public SimpleStringProperty nameProperty() {
        return name;
    }

    public SimpleStringProperty albumNameProperty() {
        return albumName;
    }



    public String getFilePath() {
        return filePath;
    }

    public String getSeconds() {
        return seconds.get();
    }

    public SimpleStringProperty secondsProperty() {
        return seconds;
    }

    public String getName() {
        return name.get();
    }

    public String getAlbumName() {
        return albumName.get();
    }

    public int getSongID() {
        return songID;
    }
}
