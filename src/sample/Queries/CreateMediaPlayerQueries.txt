Create database Mediaplayer


Create table TblArtist
(
FldArtistID int not null unique,
FldName nchar(20),
Primary key(FldArtistID) 
);

Create table TblAlbum
(
FldAlbumID int not null unique,
FldAlbumName nchar(50),
FldArtistID int,
FldReleaseYear int,
Primary key(FldAlbumID)
);

Create table TblSong
(
FldSongID int not null unique,
FldSongName nchar(50),
FldAlbumID int,
FldVideoPath nchar(200),
FldLength int,
FldThumbnailPath nchar(200),
Primary key(FldSongID)
);

Create table TblPlaylistSong
(
FldPlaylistSongID int identity,
FldSongID int,
FldPlaylistID int,
Primary key(FldPlaylistSongID)
);

Create table TblPlaylist
(
FldPlaylistID int identity,
FldPlaylistName nchar(50),
FldOwnerID int,
FldLastSongIDPlayed int,
Primary key(FldPlaylistID) 
);





