insert into TblArtist(FldArtistID, FldName)
values(1, 'AC/DC'),
(2, 'Ozzy Osborne'),
(3, 'Iron Maiden'),
(4, 'Night Wish'),
(5, 'Def Leppard'),
(6, 'Judas Priest'),
(7, 'Evanescence');


insert into TblAlbum(FldAlbumID, FldAlbumName, FldArtistID, FldReleaseYear)
values(1, 'Back in Black', 1, 1980),
(2, 'Blizzard of Oz', 2, 1980),
(3, 'Brave new World', 3, 2000),
(4, 'Century Child', 4, 2002),
(5, 'Dance of Death', 3, 2003),
(6, 'Dark Passion Play', 4, 2007),
(7, 'Fallen', 7, 2003),
(8, 'Fear of the Dark', 3, 1992),
(9, 'High Voltage', 1, 1978),
(10, 'Highway to Hell', 1, 1979),
(11, 'Hysteria', 5, 1987),
(12, 'The Open Door', 7, 2006),
(13, 'Painkiller', 6, 1990);

insert into TblUser(FldUserID, FldName)
values(1, 'Allan'),
(2, 'Dennis'),
(3, 'Niels'),
(4, 'Oliver');

insert into TblSong(FldSongID, FldSongName, FldAlbumID, FldVideoPath, FldLength)
values(1, 'Hells Bells', 1, 'src\sample\media\Back in Black\Hells_Bells.mp4', 309),
(2, 'Shoot to Thrill', 1,  'src\sample\media\Back in Black\Shoot_To_Thrill.mp4', 320),
(3, 'Back in Black', 1, 'src\sample\media\Back in Black\Back_in_Black.mp4', 254),
(4, 'You shook me all night', 1, 'src\sample\media\Back in Black\You_Shook_Me_All_Night_Long.webm', 212),
(5, 'I dont know', 2, 'src\sample\media\Blizzard of Ozz\I_Dont_Know.mp4', 293),
(6, 'Crazy Train', 2, 'src\sample\media\Blizzard of Ozz\Crazy_Train.webm', 326),
(7, 'Mr Crowley', 2, 'src\sample\media\Blizzard of Ozz\Mr_Crowley.mp4', 368),
(8, 'Suicide Solution', 2, 'src\sample\media\Blizzard of Ozz\Suicide_Solution.mp4', 502),
(9, 'Dream of mirrors', 3, 'src\sample\media\Brave new world\yt1s.com - Iron Maiden  Dream Of Mirrors.mp4', 561),
(10, 'Ghost of the navigator', 3, 'src\sample\media\Brave new world\yt1s.com - Iron Maiden  Ghost Of The Navigator_360p.mp4', 410),
(11, 'The wicker man', 3, 'src\sample\media\Brave new world\yt1s.com - Iron Maiden  The Wicker Man Official Video.mp4', 275),
(12, 'Ever dream', 4, 'src\sample\media\Century Child\yt1s.com - Ever Dream.mp4', 282),
(13, 'End of all hope', 4, 'src\sample\media\Century Child\yt1s.com - Nightwish  End Of All Hope OFFICIAL VIDEO.mp4', 235),
(14, 'Dance of Death', 5, 'src\sample\media\Dance of death\yt1s.com - Dance of Death 2015 Remaster.mp4', 516),
(15, 'Wildest dreams', 5, 'src\sample\media\Dance of death\yt1s.com - Iron Maiden  Wildest Dreams_360p.mp4.webm', 232),
(16, 'Eva', 6, 'src\sample\media\Dark passion play\yt1s.com - Eva  Nightwish_360p.mp4', 264),
(17, 'Amaranth', 6, 'src\sample\media\Dark passion play\yt1s.com - Nightwish  Amaranth OFFICIAL VIDEO.mp4', 233),
(18, 'The islander', 6, 'src\sample\media\Dark passion play\yt1s.com - Nightwish  The Islander OFFICIAL VIDEO.mp4', 309),
(19, 'Going under', 7, 'src\sample\media\Fallen\yt1s.com - Evanescence  Going Under Official Music Video.mp4.webm', 218),
(20, 'Haunted', 7, 'src\sample\media\Fallen\yt1s.com - Evanescence  Haunted Lyrics_360p.mp4', 196),
(21, 'Imaginary', 7, 'src\sample\media\Fallen\yt1s.com - Evanescence  Imaginary lyrics_360p.mp4', 264),
(22, 'Be quick or be dead', 8, 'src\sample\media\Fear of the dark\yt1s.com - Iron Maiden  Be Quick Or Be Dead Official Video.mp4', 204),
(23, 'Chains of misery', 8, 'src\sample\media\Fear of the dark\yt1s.com - Iron Maiden  Chains of Misery Lyrics_360p.mp4', 217),
(24, 'Wasting love', 8, 'src\sample\media\Fear of the dark\yt1s.com - Iron Maiden  Wasting Love Official Video.mp4', 346),
(25, 'Its a long way to the top...', 9, 'src\sample\media\High voltage\yt1s.com - ITS A LONG WAY TO THE TOP IF YOU WANNA ROCK N ROLL  AC DC.mp4', 308),
(26, 'Highway to hell', 10, 'src\sample\media\Highway to Hell\yt1s.com - ACDC  Highway to Hell Official Video.mp4', 207),
(27, 'Hysteria', 11, 'src\sample\media\Hysteria\yt1s.com - Def Leppard  Hysteria Long Version.mp4', 354),
(28, 'Love bites', 11, 'src\sample\media\Hysteria\yt1s.com - Def Leppard  Love Bites.mp4.webm', 321),
(29, 'Pour some sugar on me', 11, 'src\sample\media\Hysteria\yt1s.com - DEF LEPPARD  Pour Some Sugar On Me Official Music Video.mp4', 355),
(30, 'Cloud nine', 12, 'src\sample\media\the Open door\yt1s.com - evanescence  Cloud nine lyrics_360p.mp4', 262),
(31, 'Like you', 12, 'src\sample\media\the Open door\yt1s.com - Evanescence  Like You  Lyrics_360p.mp4', 285),
(32, 'Lithium', 12, 'src\sample\media\the Open door\yt1s.com - Evanescence  Lithium Official Music Video.mp4', 228),
(33, 'Night crawler', 13, 'src\sample\media\Pain killer\yt1s.com - Judas Priest  Night Crawler Epitaph.mp4', 383),
(34, 'Painkiller', 13, 'src/sample/media/Pain killer/yt1s.com - Judas Priest  Painkiller.mp4', 368)
















