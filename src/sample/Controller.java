package sample;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.io.*;
import java.net.*;
import java.util.*;

public class Controller implements Initializable {
    @FXML
    private MediaView mediaV;

    @FXML
    private Button play;

    @FXML
    private TextField searchBar, playListText;

    @FXML
    private TableView<Song> songTableView;

    @FXML
    private TableView<PlayList> playListTable;

    @FXML
    private Slider volumeSlider;

    @FXML
    private ComboBox searchComboBox;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private Slider progressSlider;

    @FXML
    private Button sceneSwitcher;

    @FXML
    private Button MinButton;

    @FXML
    private Label songName;

    @FXML
    private Label songDuration;

    @FXML
    private Label currentTime;


    //song table column declaration
    private TableColumn<Song, String> songNameColumn;
    private TableColumn<Song, String> albumNameColumn;
    private TableColumn<Song, Integer> durationColumn;

    //playlist table column declaration
    private TableColumn<PlayList, String> playListNameColumn;
    private TableColumn<PlayList, Integer> playlistOwnerColumn;


    //Global Variables
    private MediaPlayer mp;
    private Media me;
    private boolean isPlaying;
    private ObservableList<Song> songs;
    private ObservableList<PlayList> playLists;
    private int selectedPlaylistID;
    private int selectedSongID;
    private ArrayList<String> pathList;
    private int playlistCounter;
    private String searchCriteria;
    private Timer timer;
    private TimerTask task;
    private boolean isFirstSong;
    private ArrayList<String> songNameList;
    private ArrayList<String> durationList;



    //scene swtich

    /**
     * This method is invoked automatically in the beginning. Used for initializing, loading data etc.
     * @param location
     * @param resources
     * Initialise the observable array lists.
     * Set the table view widths
     * Set the column widths
     * Add the columns to the table views
     * Create the event listeners
     */
    public void initialize(URL location, ResourceBundle resources) {
        //table view has to be set with an observablelist, so we construct one here
        songs = FXCollections.observableArrayList();
        playLists = FXCollections.observableArrayList();
        //SETUP THE TABLEVIEW TO SEARCH FOR SONGS
        //Create th columns in code
        songNameColumn = new TableColumn<>("Song Name");
        albumNameColumn = new TableColumn<>("Album Name");
        durationColumn = new TableColumn<>("Duration");

        //Disable sorting
        songNameColumn.setSortable(false);
        albumNameColumn.setSortable(false);
        durationColumn.setSortable(false);



        //set size of song table
        songTableView.setPrefWidth(360);
        songNameColumn.setPrefWidth(160);
        albumNameColumn.setPrefWidth(120);
        durationColumn.setPrefWidth(80);

        //Add the columns to the tableview
        songTableView.getColumns().add(songNameColumn);
        songTableView.getColumns().add(albumNameColumn);
        songTableView.getColumns().add(durationColumn);

        //Link the columns to the attributes of the song class
        songNameColumn.setCellValueFactory(x -> x.getValue().nameProperty());
        albumNameColumn.setCellValueFactory(x -> x.getValue().albumNameProperty());
        durationColumn.setCellValueFactory(new PropertyValueFactory<Song, Integer>("seconds"));

        //SETUP THE TABLEVIEW TO CREATE AND DISPLAY PLAYLISTS
        playListNameColumn = new TableColumn<>("Playlist Name");
        playlistOwnerColumn = new TableColumn<>("Playlist OwnerID");

        //Set size of playlist table
        playListTable.setPrefWidth(125);
        playListNameColumn.setPrefWidth(120);

        //disable sorting
        playListNameColumn.setSortable(false);

        //add the columns to the tableview
        playListTable.getColumns().addAll(playListNameColumn);

        //Link the columns to the attributes of the playlist class
        playListNameColumn.setCellValueFactory(x -> x.getValue().playListNameProperty());
        playlistOwnerColumn.setCellValueFactory(new PropertyValueFactory<>("ownerID"));

        //Going forward, can we use just one table view? Switch between sets of columns

        //Add items to the combobox

        searchComboBox.getItems().add("Song name");
        searchComboBox.getItems().add("Album name");
        searchComboBox.getItems().add("Artist name");

        searchCriteria = "Song name";
        isFirstSong = true;

        //Combo box event listener to select the search critera
        searchComboBox.setOnAction((Event -> {
            searchCriteria = (String)searchComboBox.getSelectionModel().getSelectedItem();
        }));

        //playlist tableview event listener to select the playlistID
        playListTable.getSelectionModel().selectedItemProperty().addListener((observableValue, s, t1) -> {
            selectPlayListID();
        });

        //slider event listener to update as the media file plays.
        progressSlider.setOnMouseClicked(mouseEvent -> {
            mp.seek(new Duration(progressSlider.getValue()*convertTimeToInt(songs.get(playlistCounter).getSeconds())*1000)); cancelTimer(); beginTimer();});

        //When the program first runs, we want to display all the songs and playlists.
        songSearcher();
        playlistSearchButton();

    }

    /**
     * Method used to close the program. The power button
     * */
    @FXML
    public void Exit(){
        System.exit(1);
    }

    /**
     * Method used to minimise the media player
     * */
    @FXML
    private void Minimize () {
        Stage stage = (Stage) MinButton.getScene().getWindow();
        stage.setIconified(true);
    }

    /**
     * @param link
     * Given the file path of a media file, this method can play the file.
     * */

    public void playLink(String link){
        if(isPlaying){ //Bug with overlapping videos resolved
            mp.stop();
        }
        // Build the path to the location of the media file
        String path = new File(link).getAbsolutePath();

        // Create new MediaPlayer and attach the media to be played
        me = new Media(new File(path).toURI().toString());

        mp = new MediaPlayer(me);
        mediaV.setMediaPlayer(mp);
        if(isFirstSong){
            volumeSlider.setValue(mp.getVolume()*100);
        }
        isFirstSong = false;
        mp.setVolume(volumeSlider.getValue()/100);

        // mp.setAutoPlay(true);
        // If autoplay is turned of the method play(), stop(), pause() etc controls how/when medias are played
        mp.setAutoPlay(false);
        isPlaying = false;


        //set volumeslider event listener. It can change the volume when any change is made to the slider
        volumeSlider.valueProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                mp.setVolume(volumeSlider.getValue()/100);
            }
        });
    }

    /**
     * This function is called when the play button is clicked.
     * */

    @FXML
    private void playButtonFunction()
    {
        // Play the mediaPlayer with the attached media
        if(isPlaying == false){
            try{
                mp.play();
            }catch (Exception e){ //In case there is no media file selected.
                System.out.println("No media file selected");
                return;
            }
            setButtonPause(); //change the image on the button
            isPlaying = true; //switch flag
            beginTimer();
        }else{
            mp.pause();
            setButtonPlay();
            isPlaying = false;
            cancelTimer();
        }
    }

    /**
     * This function is called when an image needs to be inserted in the imageview of the play button.
     * */
    public void setButtonPlay(){
        //Adding icon to button
        ImageView imageView = new ImageView("sample/Pictures/Play.png");
        imageView.setFitWidth(40); //set size
        imageView.setFitHeight(40);
        play.setGraphic(imageView); //add the image view to the button
    }

    /**
     * This function is called when an image needs to be inserted in the imageview of the pause button.
     * */
    public void setButtonPause(){
        ImageView imageView = new ImageView("sample/Pictures/pause.png");
        imageView.setFitWidth(40); //set size
        imageView.setFitHeight(40);
        play.setGraphic(imageView); //add the image view to the button
    }

    /**
     * This function is called when an image needs to be inserted in the imageview of the stop button.
     * */
    public void stopButton(){
        mp.stop(); //stop the player
        progressSlider.setValue(0); //update the progress slider
        progressBar.setProgress(0); //update the progress bar
        setButtonPlay(); //change the image on the button
        isPlaying = false; //switch the flag
    }

    /**
     * Unused function. It is how data is extracted from the database
     * */
    public static void getDataFromDB(){
        do{
            String data = DB.getData();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                System.out.println(data);
            }
        }while (true);
    }

    /**
     * @param songKeyWord
     * Given a String, this function can find song names which contain that string.
     * It also displays the searched songs in the tableview.
     * */
    public void getSearchedSongs(String songKeyWord){
        //Clear the tableview everytime we search for new items. We don't want to add to the list
        songTableView.getItems().clear(); //clear the table view
        playlistCounter = 0; //reset the counter
        pathList = new ArrayList<>(); //reset all the arraylists
        songNameList = new ArrayList<>();
        durationList = new ArrayList<>();
        //Local variables
        int marker = 0; //this marker is needed to keep track of which item is being extracted from the database

        //Attributes of the song. The select will get the values and they will be stored until we can create an object
        String songName ="";
        String albumName = "";
        String length = "";
        int songID = 0;
        String filePath = "";


        DB.selectSQL("select FldSongName, FldAlbumName, FldLength, FldVideoPath, FldSongID from \n" +
                "TblAlbum inner join TblSong on \n" +
                "TblAlbum.FldAlbumID = TblSong.FldAlbumID\n" +
                "where FldSongName like '%" + songKeyWord + "%'");

        do{
            String data = DB.getData().trim();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                if(marker%5 == 0){ //retrieving 4 items from the DB, so every 4th item is the song name, album name, etc
                    songName = data;
                    songNameList.add(songName);
                }else if(marker%5 == 1){
                    albumName = data;
                }else if(marker%5 == 2){
                    durationList.add(data);
                    length = convertTimeForTable(Integer.parseInt(data));
                }else if(marker%5 == 3){
                    filePath = data;
                    pathList.add(filePath);
                }else if(marker%5 == 4){
                    songID = Integer.parseInt(data);
                }
                marker++;
            }
            if(marker%5 == 0){ //At the end of getting 4 items from the DB, we create a class with all that data and add it to the observable list
                songs.add(new Song(songName, albumName, length, filePath, songID));
            }
        }while (true);

        //Set the items of the tableview with the observable list
        songTableView.setItems(songs);
    }

    /**
     *This is the function which is called when the search button is clicked and the combo box says search by song name
     * */
    public void songSearcher(){ //The function which works with the button. button functions can't have arguments
        String labelsearch = searchBar.getText().trim();
        getSearchedSongs(labelsearch);
    }

    public void playSelected(){
        String path = "";
        int i;
        for (i = 0; i < songTableView.getItems().size(); i++) { //scan through all the rows of the table
            if(songTableView.getSelectionModel().isSelected(i)){ //The selected element
                path = songTableView.getItems().get(i).getFilePath().trim(); //Store the path
                break;
            }
        }
        try{
            playLink(path); //play the video with the given file path
        }catch (Exception e){
            System.out.println("No song selected");
        }

    }

    /**
     * @param albumKeyword
     * Search for songs in an album or multiple albums, given a String which is part of the album's name
     * It also displays the searched songs in the tableview.
     * */
    public void searchSongsOfAlbum(String albumKeyword){
        //Clear the tableview everytime we search for new items. We don't want to add to the list
        songTableView.getItems().clear(); //reset the table view
        playlistCounter = 0; //reset the counter
        pathList = new ArrayList<>();
        songNameList = new ArrayList<>();
        durationList = new ArrayList<>(); //reset all the array lists
        //local variable. This variable is used to keep track of which element we are getting from the database.
        int marker = 0;

        //The song class attributes.
        String songName ="";
        String albumName = "";
        String length = "";
        String filePath = "";
        int songID = 0;
        //select query to get the data from the database.
        DB.selectSQL("select FldSongName, FldAlbumName, FldLength, FldVideoPath, FldSongID from TblAlbum inner join \n" +
                "TblSong on \n" +
                "TblAlbum.FldAlbumID = TblSong.FldAlbumID\n" +
                "where FldAlbumName like '%"+albumKeyword+"%'"); //searching using the album keyword

        //We get data from the database in a certain order. Given the order, we can store it in the right variables
        do{
            String data = DB.getData().trim();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                if(marker%5 == 0){
                    songName = data;
                    songNameList.add(songName);
                }else if(marker%5 == 1){
                    albumName = data;
                }else if(marker%5 == 2){
                    durationList.add(data);
                    length = convertTimeForTable(Integer.parseInt(data));
                }else if(marker%5 == 3){
                    filePath = data;
                    pathList.add(filePath);
                }else if(marker%5 == 4){
                    songID = Integer.parseInt(data);
                }
                marker++;
            }
            if(marker%5 == 0){
                songs.add(new Song(songName, albumName, length, filePath, songID)); //create an object with all the data.
            }
        }while (true); //repeat until there is no data from the database
        songTableView.setItems(songs);
    }

    /**
     * This method is called when the search button is clicked and the album value is selected in the combo box.
     * */
    public void albumSearcher(){ //The function which is called when the button is clicked
        String labelsearch = searchBar.getText().trim();
        searchSongsOfAlbum(labelsearch);
    }
    /**
     * @param artistkeyWord
     * Given a String, this method can search for song names which contain the keyword.
     * It also displays the searched songs in the tableview.
     * */
    public void searchSongsOfArtist(String artistkeyWord){
        //Clear the tableview everytime we search for new items. We don't want to add to the list
        songTableView.getItems().clear();
        playlistCounter = 0; //reset the counter
        pathList = new ArrayList<>();
        songNameList = new ArrayList<>();
        durationList = new ArrayList<>(); //reset the arraylists

        int marker = 0; //This marker keeps track of the data the method receives from the database

        String songName ="";
        String albumName = "";
        String length = "";
        String filePath = "";
        int songID = 0; //initialise all the local variables
        DB.selectSQL("select FldSongName, FldAlbumName, FldLength, FldVideoPath, FldSongID " +
                "from TblArtist inner join \n" +
                "(TblAlbum inner join TblSong on \n" +
                "TblAlbum.FldAlbumID = TblSong.FldAlbumID) on \n" +
                "TblArtist.FldArtistID = TblAlbum.FldArtistID\n" +
                "where TblArtist.FldName like '%"+artistkeyWord+"%'");

        do{
            String data = DB.getData().trim();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                if(marker%5 == 0){
                    songName = data;
                    songNameList.add(songName);
                }else if(marker%5 == 1){
                    albumName = data;
                }else if(marker%5 == 2){
                    durationList.add(data);
                    length = convertTimeForTable(Integer.parseInt(data));
                }else if(marker%5 == 3){
                    filePath = data;
                    pathList.add(filePath);
                }else if(marker%5 == 4){
                   songID = Integer.parseInt(data);
                }
                marker++;
            }
            if(marker%5 == 0){
                songs.add(new Song(songName, albumName, length, filePath, songID)); //create an object with the data and add it to the observable array list
            }
        }while (true);
        songTableView.setItems(songs); //add the observable array list to the tableview
    }
    /**
     * This function is called when the search button is clicked and the combobox value is 'Artist'.
     * */
    public void artistSearcher(){
        String labelsearch = searchBar.getText().trim();
        searchSongsOfArtist(labelsearch); //using the string in the label, perform a search.
    }


    /**
     * This function is called when the search button is clicked.
     * It will select which search function to call.
     * */
    public void universalSongSearcher(){
        if(searchCriteria.equals("Song name")){ //depending on the search criteria, select the correct function
            songSearcher();
        }else if(searchCriteria.equals("Album name")){
            albumSearcher();
        }else if(searchCriteria.equals("Artist name")){
            artistSearcher();
        }
    }


    /**
     * This function is called we click the button to create a playlist.
     * It will insert a row into the database to create the playlist.
     * */
    public void createPlaylistInDB(){


        playListTable.getItems().clear(); //reset the table
        String playlistName = playListText.getText().trim(); //get the string from the label
        PlayList playList = new PlayList(playlistName, 1, 0); //ID doesn't matter here
        DB.insertSQL("insert into TblPlaylist(FldPlaylistName, FldOwnerID)\n" +
                "values('" + playList.getPlayListName() + " ',"+  playList.getOwnerID() +")"); //insert the new playlist into the database
        playlistSearch(""); //display all the playlists.
    }

    /**
     * @param playlistKeyWord
     * Given a string, this method can search for playlist names which contain the string.
     * It also adds the playlist names to the tablview.
     * */
    public void playlistSearch(String playlistKeyWord){
        playListTable.getItems().clear(); //reset the table
        String playlistName = ""; //initialise all the local variables
        int ownerID = 0;
        int marker = 0;
        int playlistID = 0;
        DB.selectSQL("select FldPlaylistName, FldOwnerID, FldPlaylistID from \n" +
                "TblPlaylist where FldPlaylistName like '%" + playlistKeyWord +"%'");
        do{
            String data = DB.getData().trim();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                if(marker%3 == 0){
                    playlistName = data;
                }else if(marker%3 == 1){
                    ownerID = Integer.parseInt(data);
                }else if(marker%3 == 2){
                    playlistID = Integer.parseInt(data);
                }
                marker++;
            }
            if(marker%3 == 0){
                playLists.add(new PlayList(playlistName, ownerID, playlistID)); //create objects with that data and add it to the observable list
            }
        }while (true);
        playListTable.setItems(playLists); //Add the observable list to the tableview
    }

    /**
     * This method is called when the playlist search button is clicked.
     * */
    public void playlistSearchButton(){
        String input = playListText.getText().trim();
        playlistSearch(input);
    }

    /*
    * To add a song to a playlist
    * A playlist needs to be selected in the playlist tableview
    * A song needs to be selected in the songs tableview
    * Click a button which adds both their IDs to the playlistsong table
    *
    * Should we check so that we don't add the same song twice?
    * */




    /**
     * This method is called when the button is clicked to add a particular song to a playlist
     * */
    public void addSongToPlaylist(){
        //Need ID of selected playlist
        //Need ID of selected song

        int songID = 0;
        int count = 0;

        //get songID of currently selected song.

        for (int i = 0; i < songTableView.getItems().size(); i++) {
            if(songTableView.getSelectionModel().isSelected(i)){
                songID = songTableView.getItems().get(i).getSongID();
                break;
            }
        }

        //Test if the song has already been added to this playlist
        DB.selectSQL("select count(FldPlaylistSongID) from \n" +
                "TblPlaylistSong where FldSongID = " + songID + "\n" +
                "and FldPlaylistID = " + selectedPlaylistID);
        do{
            String data = DB.getData().trim();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                count = Integer.parseInt(data);
            }
        }while (true);

        if(count >0){
            System.out.println("already in playlist");
            return;
        }

        //playlistID has already been selected
        if(songID == 0 || selectedPlaylistID == 0){
            System.out.println("No selection");
            return;
        }

        DB.insertSQL("insert into TblPlaylistSong(FldSongID, FldPlaylistID)\n" +
                "values("+ songID + "," + selectedPlaylistID +")");

    }
    /**
     * In order to select a playlist to play or delete, we need the ID.
     * This method gets the ID from the table view which got it from the database.
     * */
    public void selectPlayListID(){
        for (int i = 0; i < playListTable.getItems().size(); i++) {
            if(playListTable.getSelectionModel().isSelected(i)){
                selectedPlaylistID = playListTable.getItems().get(i).getPlaylistID();
            }
        }
    }

    /**
     * First the ID of the playlist is selected using a separate method.
     * Delete queries are executed using the ID to delete the playlist from 2 tables.
     * */
    public void deletePlaylist(){
        selectPlayListID();
        DB.deleteSQL("delete from TblPlaylistSong where FldPlaylistID = " + selectedPlaylistID); //if the tables are linked, this one needs to be executed first
        DB.deleteSQL("delete from TblPlaylist where FldPlaylistID = " + selectedPlaylistID);
        playlistSearch("");
    }

    /**
     * This method is called when the button is clicked to display songs in the selected playlist.
     * It adds the songs to the table view.
     * */
    public void displaySongsInPlaylist(){
        songTableView.getItems().clear(); //reset the table
        playlistCounter = 0;
        pathList = new ArrayList<>(); //reset the arraylists
        songNameList = new ArrayList<>();
        durationList = new ArrayList<>();
        int marker = 0;
        String songName ="";
        String albumName = "";
        String length = "";
        String filePath = "";
        int songID = 0;
        DB.selectSQL("select T2.FldSongName, T1.FldAlbumName, T2.FldLength, T2.FldVideoPath, T2.FldSongID\n" +
                "from TblAlbum T1\n" +
                "inner join\n" +
                "(TblSong T2 inner join \n" +
                "(TblPlaylistSong T3 inner join TblPlaylist T4 on T3.FldPlaylistID = T4.FldPlaylistID\n" +
                ") on T2.FldSongID = T3.FldSongID  \n" +
                ") on T1.FldAlbumID = T2.FldAlbumID where T4.FldPlaylistID = " + selectedPlaylistID);
        do{
            String data = DB.getData().trim();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                if(marker%5 == 0){
                    songName = data;
                    songNameList.add(songName);
                }else if(marker%5 == 1){
                    albumName = data;
                }else if(marker%5 == 2){
                    durationList.add(data);
                    length = convertTimeForTable(Integer.parseInt(data));
                }else if(marker%5 == 3){
                    filePath = data;
                    pathList.add(filePath);
                }else if(marker%5 == 4){
                    songID = Integer.parseInt(data);
                }
                marker++;
            }
            if(marker%5 == 0){
                songs.add(new Song(songName, albumName, length, filePath, songID)); //add data to the observable list
            }
        }while (true);
        songTableView.setItems(songs); //add the observable list to the tableview


    }

    /**
     * The ID of a song is selected from the tableview.
     * This ID was extracted from the database.
     * */
    public void selectSongID(){
        int i;
        for (i = 0; i < songTableView.getItems().size(); i++) {
            if(songTableView.getSelectionModel().isSelected(i)){
                selectedSongID = songTableView.getItems().get(i).getSongID();
                return;
            }
        }
    }

    /**
     *This method is called when the button is clicked to delete a song from the database.
     *A method is called to get the ID of the song to be deleted. And then a delete query is executed.
     * */
    public void deleteSongFromPlaylist(){
        selectSongID();
        DB.deleteSQL("delete from TblPlaylistSong where FldSongID = " + selectedSongID + " and FldPlaylistID = " + selectedPlaylistID); // It will delete the song from all playlists.
        displaySongsInPlaylist(); //after deleting it, display the remaining songs
    }

    public void setListToPlay(){
        pathList = new ArrayList<>();
        DB.selectSQL("select FldVideoPath from TblPlaylistSong inner join TblSong on TblPlaylistSong.FldSongID = TblSong.FldSongID where FldPlaylistID = " + selectedPlaylistID);
        do{
            String data = DB.getData().trim();
            if (data.equals(DB.NOMOREDATA)){
                break;
            }else{
                pathList.add(data);
            }
        }while (true);
    }

    /**
     * This method is called when we click the button to 'play all'.
     * It selects the starting point which is the currently selected song.
     * And then it plays the whole list from the currently selected song.
     * */
    public void playPlayListFromPoint(){ //Play the playlist from currently selected song.
        for (int i = 0; i < songTableView.getItems().size(); i++) {
            if(songTableView.getSelectionModel().isSelected(i)){
                playlistCounter = i;
                break;
            }
        }
        playWholeListOfPlaylist();
    }

    /**
     * This method is called to play the whole playlist after the starting point has been selected.
     * At the end of the song, the next song is played.
     * */
    public void playWholeListOfPlaylist(){
        //Set the playlist counter here
        playLink(pathList.get(playlistCounter));
        songName.setText(songNameList.get(playlistCounter));
        songDuration.setText(convertTimeForTable(Integer.parseInt(durationList.get(playlistCounter))));
        mp.setOnEndOfMedia(() ->{ //when the song ends, we play the next song.
            cancelTimer();
            progressSlider.setValue(0);
            playlistCounter++;
            setButtonPlay();
            isPlaying = false;
            if(playlistCounter >= pathList.size()){ //if there are no more songs to play, end here
                playlistCounter = 0;
            }
            playWholeListOfPlaylist();

        });

        playButtonFunction();
    }


    /**
     * This method is called when we click the button to play the next song.
     * */
    public void playNextSong(){

        if(playlistCounter+1 >= pathList.size()){
            playlistCounter = -1; //Skipping the last song in the playlist should play the first song
        }
        cancelTimer();
        playlistCounter++;
        progressSlider.setValue(0);
        playWholeListOfPlaylist();
    }

    /**
     * This method is called when we click the button to play the previous song.
     * */
    public void playPreviousSong(){
        if(playlistCounter == 0){
            playlistCounter = pathList.size(); //Play the last song in the playlist if you press previous on the first song.
        }
        cancelTimer();
        playlistCounter--;
        progressSlider.setValue(0);
        playWholeListOfPlaylist();
    }

    /**
     * The timer is used in other functions, like the progress bar, the slider and label which contains the current time of the song.
     * */
    public void beginTimer(){
        timer = new Timer();
        task = new TimerTask() {
            public void run(){
                double current = mp.getCurrentTime().toSeconds();
                double end = me.getDuration().toSeconds();
                progressBar.setProgress(current/end);
                progressSlider.setValue(current/end);
                Platform.runLater(()->currentTime.setText(convertTimeForTable((int)current)));
                if (current/end == 1){
                    cancelTimer();
                }
            }
        };
        timer.scheduleAtFixedRate(task,0,1000);
    }

    public void cancelTimer(){
        timer.cancel();
    }

    /**
     * @param num
     * @return The time in m:ss format
     * Given an integer, this function returns a string which is in the minutes:seconds format.
     * */
    public String convertTimeForTable(int num){
        int minutes;
        int seconds;
        minutes = num/60;
        seconds = num%60;
        if(seconds <10){
            return minutes + ":0" + seconds;
        }
        return minutes + ":" + seconds;
    }

    /**
     * @param time
     * @return The time in seconds only. Integer format
     * Given the time as a string in the minutes:seconds format, this method returns the seconds value in integer format.
     * */
    public int convertTimeToInt(String time){
        return Integer.parseInt(time.charAt(0) + "")*60 + Integer.parseInt(time.charAt(2) + "")*10 + Integer.parseInt(time.charAt(3) +"");
    }

}
